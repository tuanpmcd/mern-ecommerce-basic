const router = require("express").Router();
const userCtrl = require("../controllers/useCtrl");
const auth = require("../middleware/auth");

router.post("/register", userCtrl.register);
router.post("/login", userCtrl.login);
router.get("/logout", userCtrl.logout);
router.get("/refresh_token", userCtrl.rf_token);
router.get("/infor", auth, userCtrl.getUser);
router.patch("/addcart", auth, userCtrl.addCart);
router.get("/history", auth, userCtrl.history);

module.exports = router;

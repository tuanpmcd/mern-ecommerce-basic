import React, { useState, useContext } from "react";
import { GlobalState } from "../../GlobalState";
import Bars from "./icon/bars.svg";
import Times from "./icon/times.svg";
import ShoppingCart from "./icon/shopping-cart.svg";
import { Link } from "react-router-dom";
import axios from "axios";

const Header = () => {
  const state = useContext(GlobalState);
  const [isLogged] = state.UserAPI.isLogged;
  const [isAdmin] = state.UserAPI.isAdmin;
  const [cart] = state.UserAPI.cart;
  const [menu, setMenu] = useState(false);

  const logoutUser = async () => {
    await axios.get("/user/logout");
    localStorage.removeItem("firstLogin");
    window.location.href = "/";
  };

  const adminRouter = () => {
    return (
      <>
        <li>
          <Link onClick={() => setMenu(!menu)} to="/create_product">
            Create product
          </Link>
        </li>
        <li>
          <Link onClick={() => setMenu(!menu)} to="/category">
            Categories
          </Link>
        </li>
      </>
    );
  };
  const loggedRouter = () => {
    return (
      <>
        <li>
          <Link onClick={() => setMenu(!menu)} to="/history">
            History
          </Link>
        </li>
        <li>
          <Link onClick={() => setMenu(!menu)} to="/" onClick={logoutUser}>
            Logout
          </Link>
        </li>
      </>
    );
  };

  const styleMenu = {
    left: menu ? 0 : "-100%",
  };

  return (
    <header>
      <div className="menu" onClick={() => setMenu(!menu)}>
        <img src={Bars} alt="" width="30" />
      </div>

      <div className="logo">
        <h2>
          <Link to="/">{isAdmin ? "Admin" : "PET.SHOP"}</Link>
        </h2>
      </div>

      {isAdmin ? (
        ""
      ) : (
        <div className="cart-icon">
          <span>{cart.length}</span>
          <Link to="/cart">
            <img src={ShoppingCart} alt="" width="30" />
          </Link>
        </div>
      )}
      <ul style={styleMenu}>
        <li>
          <Link onClick={() => setMenu(!menu)} to="/">
            {isAdmin ? "Products" : "Shop"}
          </Link>
        </li>
        {isAdmin && adminRouter()}

        {isLogged ? (
          loggedRouter()
        ) : (
          <li>
            <Link onClick={() => setMenu(!menu)} to="/login">
              Login / Register
            </Link>
          </li>
        )}

        <li onClick={() => setMenu(!menu)}>
          <img src={Times} alt="" width="30" className="menu" />
        </li>
      </ul>
    </header>
  );
};

export default Header;

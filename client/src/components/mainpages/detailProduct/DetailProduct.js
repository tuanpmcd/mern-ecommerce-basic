import React, { useState, useContext, useEffect } from "react";
import { useParams } from "react-router-dom";
import { GlobalState } from "../../../GlobalState";
import { Link } from "react-router-dom";
import ProductItem from "../utils/productItem/ProductItem";

const DetailProduct = () => {
  const params = useParams();
  const state = useContext(GlobalState);
  const [products] = state.ProductsAPI.products;

  const addCart = state.UserAPI.addCart;
  const [detailProduct, setDetailProduct] = useState([]);

  useEffect(() => {
    if (params.id) {
      products.forEach((product) => {
        if (product._id === params.id) setDetailProduct(product);
      });
    }
  }, [params.id, products]);
  if (detailProduct.length === 0) return null;
  return (
    <>
      <div className="detail">
        <img src={detailProduct.images.secure_url} alt="" />
        <div className="box-detail">
          <div className="row">
            <h3>{detailProduct.title}</h3>
            <h6>#ID: {detailProduct.product_id}</h6>
          </div>
          <span>Price: ${detailProduct.price}</span>
          <p>{detailProduct.description}</p>
          <p>{detailProduct.content}</p>
          <p className="sold">Sold: {detailProduct.sold}</p>
          <Link
            to="#!"
            className="cart"
            onClick={() => addCart(detailProduct)}
          >
            + add to cart
          </Link>
        </div>
      </div>
      <div>
        <h2>Related products:</h2>
        <div className="products">
          {products.map((product) => {
            return product.category === detailProduct.category ? (
              <ProductItem product={product} key={product._id} />
            ) : null;
          })}
        </div>
      </div>
    </>
  );
};

export default DetailProduct;

import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { GlobalState } from "../../../../GlobalState";

function BtnRender({ product, deleteProduct }) {
  const state = useContext(GlobalState);
  const [isAdmin] = state.UserAPI.isAdmin;
  const addCart = state.UserAPI.addCart;

  return (
    <div className="row_btn">
      {isAdmin ? (
        <>
          <Link
            id="btn_buy"
            className="btn"
            to="#!"
            onClick={() => deleteProduct(product._id, product.images.public_id)}
          >
            Delete
          </Link>
          <Link
            id="btn_view"
            className="btn"
            to={`/edit_product/${product._id}`}
          >
            Edit
          </Link>
        </>
      ) : (
        <>
          <Link
            id="btn_buy"
            className="btn"
            to="#!"
            onClick={() => addCart(product)}
          >
            Buy
          </Link>
          <Link id="btn_view" className="btn" to={`/detail/${product._id}`}>
            View
          </Link>
        </>
      )}
    </div>
  );
}

export default BtnRender;
